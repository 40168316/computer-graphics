#pragma once

// Calum Templeton - 40168316
#include "Vector3.h" // Including the header file Vector3.h
#include "Matrix4.h"
#include <math.h>

// Matrix4 is created with no nothing assigned 
Matrix4::Matrix4(){}

// Matrix4 is created with numbers assigned in a loop
Matrix4::Matrix4(const Matrix4& rhs)
{
	for (int i = 0; i < 16; i++)
	{
		this->m[i] = rhs.m[i];
	}
}

// Matrix4 is created with numbers assigned
Matrix4::Matrix4(float _00, float _10, float _20, float _30,
	float _01, float _11, float _21, float _31,
	float _02, float _12, float _22, float _32,
	float _03, float _13, float _23, float _33)
{
	m[0] = _00, m[4] = _10, m[8] = _20, m[12] = _30,
	m[1] = _01, m[5] = _11, m[9] = _21, m[13] = _31,
	m[2] = _02, m[6] = _12, m[10] = _22, m[14] = _32,
	m[3] = _03, m[7] = _13, m[11] = _23, m[15] = _33;
}

float& Matrix4::operator[] (int index)
{
	return m[index];
}

const float& Matrix4::operator[] (int index) const
{
	return m[index];
}

// Creating a zero 4x4 matrix
Matrix4 Matrix4::Zero()
{
	Matrix4 z;
	for (int i = 0; i < 16; i++)
	{
		z[i] = 0;
	}
	return z;
}

// Creating a 4x4 identity matrix
Matrix4 Matrix4::Identity()
{
	Matrix4 m;
	m[0] = 1, m[4] = 0, m[8] = 0, m[12] = 0,
	m[1] = 0, m[5] = 1, m[9] = 0, m[13] = 0,
	m[2] = 0, m[6] = 0, m[10] = 1, m[14] = 0,
	m[3] = 0, m[7] = 0, m[11] = 0, m[15] = 1;

	return m;
}

// Assign mat to matrix t then complete transpose, which is flip of triangles, with temp
Matrix4 Matrix4::Transpose(const Matrix4& mat)
{
	Matrix4 t;
	for (int i = 0; i < 16; i++)
	{
		t[i] = mat[i];
	}

	float temp = t[4];
	t[4] = t[1];
	t[1] = temp;

	temp = t[8];
	t[8] = t[2];
	t[2] = temp;

	temp = t[12];
	t[12] = t[3];
	t[3] = temp;

	temp = t[9];
	t[9] = t[6];
	t[6] = temp;

	temp = t[13];
	t[13] = t[7];
	t[7] = temp;

	temp = t[14];
	t[14] = t[11];
	t[11] = temp;

	return t;
}

// Setting the Translation values from a indentity matrix4
Matrix4 Matrix4::SetTranslation(const Vector3& translation)
{
	Matrix4 trans = Matrix4::Identity();
	trans[12] = translation.x;
	trans[13] = translation.y;
	trans[14] = translation.z;
	return trans;
}

// Getting the Translation values from a vector3
Vector3 Matrix4::GetTranslation(const Matrix4& mat)
{
	Vector3 vec3 = Vector3();
	vec3.x = mat[12];
	vec3.y = mat[13];
	vec3.z = mat[14];
	return vec3;
}

// Setting the SetScale method
Matrix4 Matrix4::SetScale(const Vector3& scale)
{
	Matrix4 m;
	m[0] = scale.x, m[4] = 0, m[8] = 0, m[12] = 0,
	m[1] = 0, m[5] = scale.y, m[9] = 0, m[13] = 0,
	m[2] = 0, m[6] = 0, m[10] = scale.z, m[14] = 0,
	m[3] = 0, m[7] = 0, m[11] = 0, m[15] = 1;

	return m;
}

// Set axis rotation with identity matrix underneath
Matrix4 Matrix4::SetRotationAxis(const Vector3& axis, float angle)
{
	Matrix4 m;
	m[0] = cos(angle) + axis.x * axis.x * (1 - cos(angle)), m[1] = axis.x * axis.y * (1 - cos(angle)) - axis.z * sin(angle), m[2] = axis.x * axis.z * (1 - cos(angle)) + axis.y * sin(angle), m[3] = 0,
	m[4] = axis.y * axis.x * (1 - cos(angle)) + axis.z * sin(angle), m[5] = cos(angle) + axis.y * axis.y * (1 - cos(angle)), m[6] = axis.y * axis.z * (1 - cos(angle)), m[7] = 0,
	m[8] = axis.z * axis.x * (1 - cos(angle)) - axis.y * sin(angle), m[9] = axis.z * axis.y * (1 - cos(angle)) + axis.x * sin(angle), m[10] = cos(angle) + axis.z * axis.z * (1 - cos(angle)), m[11] = 0,
	m[12] = 0, m[13] = 0, m[14] = 0, m[15] = 1;

	return m;
}

// Transformpoint
Vector3 Matrix4::TransformPoint(const Matrix4& mat, const Vector3& p)
{
	float x = (mat[0] * p.x) + (mat[4] * p.y) + (mat[8] * p.z) + mat[12];
	float y = (mat[1] * p.x) + (mat[5] * p.y) + (mat[9] * p.z) + mat[13];
	float z = (mat[2] * p.x) + (mat[6] * p.y) + (mat[10] * p.z) + mat[14];

	return Vector3(x, y, z);
}

// TransformDirection
Vector3 Matrix4::TransformDirection(const Matrix4& mat, const Vector3& n)
{
	float x = (mat[0] * n.x) + (mat[4] * n.y) + (mat[8] * n.z);
	float y = (mat[1] * n.x) + (mat[5] * n.y) + (mat[9] * n.z);
	float z = (mat[2] * n.x) + (mat[6] * n.y) + (mat[10] * n.z);

	return Vector3(x, y, z);
}

// Matrix 4x4 multiply method
Matrix4 operator*(const Matrix4& lhs, const Matrix4& rhs)
{
	Matrix4 m;
	m[0] = (lhs.m[0] * rhs.m[0]) + (lhs.m[4] * rhs.m[1]) + (lhs.m[8] * rhs.m[2]) + (lhs.m[12] * rhs.m[3]), m[4] = (lhs.m[0] * rhs.m[4]) + (lhs.m[4] * rhs.m[5]) + (lhs.m[8] * rhs.m[6]) + (lhs.m[12] * rhs.m[7]), m[8] = (lhs.m[0] * rhs.m[8]) + (lhs.m[4] * rhs.m[9]) + (lhs.m[8] * rhs.m[10]) + (lhs.m[12] * rhs.m[11]), m[12] = (lhs.m[0] * rhs.m[12]) + (lhs.m[4] * rhs.m[13]) + (lhs.m[8] * rhs.m[14]) + (lhs.m[12] * rhs.m[15]),
	m[1] = (lhs.m[1] * rhs.m[0]) + (lhs.m[5] * rhs.m[1]) + (lhs.m[9] * rhs.m[2]) + (lhs.m[13] * rhs.m[3]), m[5] = (lhs.m[1] * rhs.m[4]) + (lhs.m[5] * rhs.m[5]) + (lhs.m[9] * rhs.m[6]) + (lhs.m[13] * rhs.m[7]), m[9] = (lhs.m[1] * rhs.m[8]) + (lhs.m[5] * rhs.m[9]) + (lhs.m[9] * rhs.m[10]) + (lhs.m[13] * rhs.m[11]), m[13] = (lhs.m[1] * rhs.m[12]) + (lhs.m[5] * rhs.m[13]) + (lhs.m[9] * rhs.m[14]) + (lhs.m[13] * rhs.m[15]),
	m[2] = (lhs.m[2] * rhs.m[0]) + (lhs.m[6] * rhs.m[1]) + (lhs.m[10] * rhs.m[2]) + (lhs.m[14] * rhs.m[3]), m[6] = (lhs.m[2] * rhs.m[4]) + (lhs.m[6] * rhs.m[5]) + (lhs.m[10] * rhs.m[6]) + (lhs.m[14] * rhs.m[7]), m[10] = (lhs.m[2] * rhs.m[8]) + (lhs.m[6] * rhs.m[9]) + (lhs.m[10] * rhs.m[10]) + (lhs.m[14] * rhs.m[11]), m[14] = (lhs.m[2] * rhs.m[12]) + (lhs.m[6] * rhs.m[13]) + (lhs.m[10] * rhs.m[14]) + (lhs.m[14] * rhs.m[15]),
	m[3] = (lhs.m[3] * rhs.m[0]) + (lhs.m[7] * rhs.m[1]) + (lhs.m[11] * rhs.m[2]) + (lhs.m[15] * rhs.m[3]), m[7] = (lhs.m[3] * rhs.m[4]) + (lhs.m[7] * rhs.m[5]) + (lhs.m[11] * rhs.m[6]) + (lhs.m[15] * rhs.m[7]), m[11] = (lhs.m[3] * rhs.m[8]) + (lhs.m[7] * rhs.m[9]) + (lhs.m[11] * rhs.m[10]) + (lhs.m[15] * rhs.m[11]), m[15] = (lhs.m[3] * rhs.m[12]) + (lhs.m[7] * rhs.m[13]) + (lhs.m[11] * rhs.m[14]) + (lhs.m[15] * rhs.m[15]);

	return m;
}