#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\euler_angles.hpp>
#include <glm\gtx\projection.hpp>

using namespace std;
using namespace glm;

#include "Vector3.h"
#include "Matrix4.h"

int main()
{
	Vector3 a(2, -7, 1);
	Vector3 b(-3, 0, 4);
	Vector3::Cross(a, b);
	Vector3::Dot(a, b);
	Vector3::Normalize(a);
	Vector3::Length(a);
	Vector3::LengthSq(a);

	Matrix4::SetRotationAxis;

	// Vectors of different lengths
	/*vec2 u(1.0, 0.0);
	vec2 v(2.0, 0.0);
	vec3 w(1.0, 0.0, 0.0);
	vec3 x(2.0, 0.0, 0.0);
	vec4 y(1.0, 0.0, 0.0, 0.0);
	vec4 z(2.0, 0.0, 0.0, 0.0);

	// Addition and Subtraction
	//vec2 t = u + v;
	vec3 s = w - x;

	// Vector Scaling
	vec3 r = 5.0f * w;
	vec3 q = w / 5.0f;

	// Length of a Vector
	float p = length(x);

	// Normalizing a Vector
	vec3 o = normalize(x);

	// Dot Product
	float n = dot(w, x);

	// Vector Projection
	vec3 m = proj(w, x);

	// Cross Product
	vec3 l = cross(w, x);

	// Declaring matrices
	mat4 aa(1.0f);
	mat4 ab(2.0f);
	mat4 ac(3.0f);
	
	// Matrix addition
	mat4 ad = ab + ac;

	// Matrix Scaling
	mat4 ae = 5.0f * ac;
	mat4 af = ac / 5.0f;

	// Matrix Multiplcation
	mat4 ag = ab * ac;

	// Matrix-Vector Multiplication
	// To times a 4x4 matrix with a vector the vector must be converted to 4d
	vec4 za = ab * vec4(x, 1.0f);

	// Transformation
	// Creating a translation matrix
	mat4 ah = translate(mat4(1.0f), vec3(1.0, 0.0, 0.0));

	// Rotation Matrix
	 mat4 ai = rotate(mat4(1.0f), pi<float>(), vec3(1.0f, 0.0f, 0.0f));

	 // Scale Matrix
	 mat4 aj = scale(mat4(1.0f), vec3(1.0, 0.0, 0.0));

	 // Combining Matrices
	 mat4 ak = aa * (ab * ac);

	// Defining Quaterions
	quat ba;

	// Quaternions for Rotations
	quat bb = rotate(quat(), pi<float>(), vec3(1.0, 0.0, 0.0));
	*/
}