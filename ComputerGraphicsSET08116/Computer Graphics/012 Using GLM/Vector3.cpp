#pragma once

// Calum Templeton - 40168316
#include "Vector3.h" // Including the header file Vector3.h
#include <Math.h> // Including math.h to be able to use square root function

// Vector is created with no nothing assigned assigned
Vector3::Vector3()
{
}

// Vector is created with floats x, y and z
Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

// Vector3 method with right hand side passed 
Vector3::Vector3(const Vector3& rhs)
{
	this->x = rhs.x;
	this->y = rhs.y;
	this->z = rhs.z;

}

// Void Vector3 method with the add equals operator 
void Vector3::operator += (const Vector3& v)
{
	x += v.x;
	y += v.y;
	z += v.z;
}

// Void Vector3 method with the subtract equals operator
void Vector3::operator-=(const Vector3& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
}

// Void Vector3 method with the times equals operator 
void Vector3::operator*=(const float s)
{
	x *= s;
	y *= s;
	z *= s;
}

// Vector3 method with the divide operator
Vector3 Vector3::operator/(const float v) const
{
	return Vector3(x/v, y/v, z/v);
}

// Vector3 method with the subtract method
Vector3 Vector3::operator-(const Vector3& v) const
{
	float xsub = x - v.x;
	float ysub = y - v.y;
	float zsub = z - v.z;

	return Vector3(xsub, ysub, zsub);
}

// Vector3 method with the addition method
Vector3 Vector3::operator+(const Vector3& v) const
{
	float xadd = x + v.x;
	float yadd = y + v.y;
	float zadd = z + v.z;

	return Vector3(xadd, yadd, zadd);
}

// Vector3 method with the multiplication method
Vector3 Vector3::operator*(const float s) const
{
	float xmul = x*s;
	float ymul = y*s;
	float zmul = z*s;

	return Vector3(xmul, ymul, zmul);
}

// Vector3 method with unary minus which flips all the signs of the vector
Vector3 Vector3::operator-() const
{
	float xchanged = -x;
	float ychanged = -y;
	float zchanged = -z;

	return Vector3(xchanged, ychanged, zchanged);
}

// Static Vector3 method with the Cross Product Calculation
Vector3 Vector3::Cross(const Vector3 &vA, const Vector3& vB)
{
	float x = vA.y*vB.z - vA.z*vB.y;
	float y = vA.z*vB.x - vB.z*vA.x;
	float z = vA.x*vB.y - vB.x*vA.y;

	return Vector3(x, y, z);
}

// Static float method with the Dot Product Calculation
float Vector3::Dot(const Vector3 &vA, const Vector3& vB)
{
	// Calculate the dot product 
	float dot = (vA.x*vB.x) + (vA.y*vB.y) + (vA.z*vB.z);

	return dot;
}
// Static Vector3 method with the Normalize Calculation
Vector3 Vector3::Normalize(const Vector3& v)
{
	// Calculate the square of x, y and z and and them together to give sumsq
	float sumsq = (v.x*v.x) + (v.y*v.y) + (v.z*v.z);
	// Find the square root of sumsq
	float sqroot = sqrt(sumsq);

	// Find the normalized values of x, y and z 
	float normx = (v.x / sqroot);
	float normy = (v.y / sqroot);
	float normz = (v.z / sqroot);

	return Vector3(normx, normy, normz);
}
// Static float method with the Length Calculation
float Vector3::Length(const Vector3& v)
{
	// Calculate the square of x, y and z and and them together to give sumsq
	float sumsq = (v.x*v.x)+(v.y*v.y)+(v.z*v.z);
	// Find the square root of sumsq
	float sqroot = sqrt(sumsq);

	return sqroot;
}

// Static float method with the Length Squared Calculation
float Vector3::LengthSq(const Vector3& v)
{
	// Calculate the square of x, y and z and and them together
	float sumsq = (v.x*v.x) + (v.y*v.y) + (v.z*v.z);

	return sumsq;
}
