#pragma once

// Calum Templeton - 40168316
#include "Vector3.h" // Including the header file Vector3.h
#include "Matrix4.h"
#include "Quaternion.h"
#include <math.h>

Quaternion::Quaternion(float xx, float yy, float zz, float ww)
{
	xx = 0;
	yy = 0;
	zz = 0;
	ww = 1;
}

Quaternion::Quaternion(const Quaternion& rhs)
{
	Quaternion q = rhs;
}

Quaternion& Quarternion::operator=(const Quaternion& rhs)
{
	Quaternion q = rhs;
	q.x = rhs.x;
	q.y = rhs.y;
	q.z = rhs.z;
	q.w = rhs.w;
}

Quaternion Quaternion::Identity()
{
	Quaternion q = (0, 0, 0, 1);
	return q;
}

Quaternion Quaternion::Conjugate(const Quaternion& q)
{
	return Quaternion(q.w, -q.x, -q.y, -q.z);
}

float Quaternion::Length(const Quaternion& q)
{
	float sumsq = (q.x*q.x) + (q.y*q.y) + (q.z*q.z) + (q.w*q.w);
	
	float sqroot = sqrt(sumsq);

	return sqroot;
}

float Quaternion::LengthSq(const Quaternion& q)
{
	float sumsq = (q.x*q.x) + (q.y*q.y) + (q.z*q.z) + (q.w*q.w);

	return sumsq;
}

Quaternion Quaternion::Normalize(const Quaternion& q)
{
	// Calculate the square of x, y, z and w and them together to give sumsq
	float sumsq = (q.x*q.x) + (q.y*q.y) + (q.z*q.z) + (q.w*q.w);
	// Find the square root of sumsq
	float sqroot = sqrt(sumsq);

	// Find the normalized values of x, y, z and w
	float normx = (q.x / sqroot);
	float normy = (q.y / sqroot);
	float normz = (q.z / sqroot);
	float normw = (q.w / sqroot);

	return Quaternion(normx, normy, normz, normw);
}

float Quaternion::Dot(const Quaternion& q1, const Quaternion& q2)
{
	// Calculate the dot product 
	float dot = (q1.x*q2.x) + (q1.y*q2.y) + (q1.z*q2.z) + (q1.w*q2.w);

	return dot;
}

Quaternion Quaternion::FromAxisAngle(const Vector3& v, float angle)
{
	float qx = v.x * sin(angle / 2);
	float qy = v.y * sin(angle / 2);
	float qz = v.z * sin(angle / 2);
	float qw = cos(angle / 2);

	return Quaternion(qx, qy, qz, qw);
}

float Quaternion::ToAxisAngle(const Quaternion& q, Vector3& v)
{
	float angle = 2 * acos(q.w);
	float x = v.x / sqrt(1 - q.w*q.w);
	float y = v.y / sqrt(1 - q.w*q.w);
	float z = v.z / sqrt(1 - q.w*q.w);

	return angle, x, y, x;
}

Matrix4 ToRotationMatrix(const Quaternion& q)
{
	Matrix4 m;
	m[0] = (1 - 2 * pow(q.y, 2)) - (2 * pow(q.z, 2)), m[4] = ((2 * q.x*q.y) - (2 * q.z*q.w)), m[8] = ((2 * q.x*q.z) + (2 * q.y*q.w)), m[12] = 0,
	m[1] = ((2 * q.x*q.y) + (2 * q.z*q.w)), m[5] = ((1 - 2 * pow(q.x, 2)) - (2 * pow(q.z, 2))), m[9] = ((2 * q.y*q.z) - (2 * q.x*q.w)), m[13] = 0,
	m[2] = ((2 * q.x*q.z) - (2 * q.y*q.w)), m[6] = ((2 * q.y*q.z) + (2 * q.x*q.w)), m[10] = ((1 - 2 * pow(q.x, 2)) - (2 * pow(q.y, 2))), m[14] = 0,
	m[3] = 0, m[7] = 0, m[11] = 0, m[15] = 1;

	return m;
}

Quaternion Quaternion::FromRotationMatrix(const Matrix4& m)
{
	Quaternion q;
	q.w = sqrt((1 + m[0] + m[5] + m[10]) / 2);
	q.x = (m[9] - m[6]) / (4 * q.w);
	q.y = (m[2] - m[8]) / (4 * q.w);
	q.z = (m[4] - m[1]) / (4 * q.w);

	return Quaternion(q.x, q.y, q.z, q.w);
}

Quaternion Quaternion::Slerp(float t, const Quaternion& p, const Quaternion& q)
{
	// New quaternion I am calculating
	Quaternion g;
	//Calculating the angle between them
	float coshalfangle = q.w*p.w + q.x*p.x + q.y*p.y + q.z*p.z;
	// Calculate temporary values.
	double halfTheta = acos(coshalfangle);
	double sinHalfTheta = sqrt(1.0 - coshalfangle*coshalfangle);
	// if theta = 180 degrees then result is not fully defined
	// we could rotate around any axis normal to qa or qb
	if (fabs(sinHalfTheta) < 0.001){ // fabs is floating point absolute
		g.w = (q.w * 0.5 + p.w * 0.5);
		g.x = (q.x * 0.5 + p.x * 0.5);
		g.y = (q.y * 0.5 + p.y * 0.5);
		g.z = (q.z * 0.5 + p.z * 0.5);
		return g;
	}
	// Caluclating the ratios need t divide by
	double ratioA = sin((1 - t) * halfTheta) / sinHalfTheta;
	double ratioB = sin(t * halfTheta) / sinHalfTheta;
	//calculate Quaternion.
	g.w = (q.w * ratioA + p.w * ratioB);
	g.x = (q.x * ratioA + p.x * ratioB);
	g.y = (q.y * ratioA + p.y * ratioB);
	g.z = (q.z * ratioA + p.z * ratioB);
	
	return g;
}


Quaternion operator*(const Quaternion& q1, const Quaternion& q2)
{
	float x = q1.x * q2.w + q1.y * q2.z - q1.z * q2.y + q1.w * q2.x;
	float y = -q1.x * q2.z + q1.y * q2.w + q1.z * q2.x + q1.w * q2.y;
	float z = q1.x * q2.y - q1.y * q2.x + q1.z * q2.w + q1.w * q2.z;
	float w = -q1.x * q2.x - q1.y * q2.y - q1.z * q2.z + q1.w * q2.w;

	return (x, y, z, x);
}

Quaternion operator*(const Quaternion& q, float s)
{
	float x = q.x * s;
	float y = q.y * s;
	float z = q.z * s;
	float w = q.w * s;

	return Quaternion(x, y, z, w);
}

Quaternion operator*(float s, const Quaternion& q)
{
	float x = s * q.x;
	float y = s * q.y;
	float z = s * q.z;
	float w = s * q.w;

	return Quaternion(x, y, z, w);
}

Quaternion operator+(const Quaternion& q1, const Quaternion& q2)
{
	float x = q1.x + q2.x;
	float y = q1.y + q2.y;
	float z = q1.z + q2.z;
	float w = q1.w + q2.w;

	return Quaternion(x, y, z, w);
}