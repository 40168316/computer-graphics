#version 440

// Incoming vertex colour
layout (location = 0) in vec4 v_colour;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
	// **************************
	// Set outgoing vertex colour
	// **************************
	colour = v_colour;
}