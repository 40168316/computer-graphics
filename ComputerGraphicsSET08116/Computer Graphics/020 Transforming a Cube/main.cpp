#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

geometry geom;
effect eff;
target_camera cam;
float theta = 0.0f;
float rho = 0.0f;
vec3 pos(0.0f, 0.0f, 0.0f);
float s = 1.0f;

bool load_content()
{
	// Create cube data - twelve triangles triangles
	// Positions
	geom.set_type(GL_QUADS);
	vector<vec3> positions
	{
		// ****************************************
		// Add the position data for triangles here
		// ****************************************
		// Front COUNTER
		vec3(1.0f, 1.0f, 0.0f),
		vec3(-1.0f, 1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f),
		// Back COUNTER
		//vec3(1.0f, -1.0f, -2.0f),
		//vec3(1.0f, 1.0f, -2.0f),
		//vec3(-1.0f, 1.0f, -2.0f),
		//vec3(-1.0f, -1.0f, -2.0f),
		// Back CLOCK
		vec3(1.0f, -1.0f, -2.0f),
		vec3(-1.0f, -1.0f, -2.0f),
		vec3(-1.0f, 1.0f, -2.0f),
		vec3(1.0f, 1.0f, -2.0f),
		// Right COUNTER
		vec3(1.0f, 1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, -2.0f),
		vec3(1.0f, 1.0f, -2.0f),
		// Left COUNTER
		vec3(-1.0f, 1.0f, 0.0f),
		vec3(-1.0f, 1.0f, -2.0f),
		vec3(-1.0f, -1.0f, -2.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		// Top COUNTER
		vec3(1.0f, 1.0f, 0.0f),
		vec3(1.0f, 1.0f, -2.0f),
		vec3(-1.0f, 1.0f, -2.0f),
		vec3(-1.0f, 1.0f, 0.0f),
		// Bottom CLOCK
		vec3(1.0f, -1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(-1.0f, -1.0f, -2.0f),
		vec3(1.0f, -1.0f, -2.0f)

	};
	// Colours
	vector<vec4> colours;
	for (auto i = 0; i < positions.size(); ++i)
		colours.push_back(vec4(1.0f, 0.0f, 0.0f, 1.0f));
	// Add to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	geom.add_buffer(colours, BUFFER_INDEXES::COLOUR_BUFFER);

	// Load in shaders
	eff.add_shader(
		"..\\resources\\shaders\\basic.vert", // filename
		GL_VERTEX_SHADER); // type
	eff.add_shader(
		"..\\resources\\shaders\\basic.frag", // filename
		GL_FRAGMENT_SHADER); // type
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(10.0f, 10.0f, 10.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	// ***********************************
	// Use keys to update transform values
	// WSAD - movement
	// Cursor - rotation
	// O decrease scale, P increase scale
	// ***********************************


	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Bind effect
	renderer::bind(eff);
	// Create MVP matrix
	// ****************************
	// Create transformation matrix
	// ****************************
	mat4 T = translate(mat4(1.0f), vec3(4.0f, 4.0f, 0.0f));
	mat4 S = scale(mat4(2.0f), vec3(s));
	mat4 R = rotate(mat4(1.0f), pi<float>(), vec3(0.0f, 0.0f, 1.0f));
	mat4 M = T * (R * S);

	auto V = cam.get_view();
	auto P = cam.get_projection();
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(
		eff.get_uniform_location("MVP"), // Location of uniform
		1, // Number of values - 1 mat4
		GL_FALSE, // Transpose the matrix?
		value_ptr(MVP)); // Pointer to matrix data
	// Render geometry
	renderer::render(geom);
	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}