#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect eff;
texture tex;
target_camera cam;

bool load_content()
{
	// Create plane mesh
	meshes["plane"] = mesh(geometry_builder::create_plane());
	// - Box
	meshes["geombx"] = mesh(geometry_builder::create_box());
	// - Tetrahedron
	meshes["geomtr"] = mesh(geometry_builder::create_tetrahedron());
	//// - Pyramid
	meshes["geompr"] = mesh(geometry_builder::create_pyramid());
	//// - Disk
	meshes["geomdk"] = mesh(geometry_builder::create_disk(20));
	//// - Cylinder
	meshes["geomcl"] = mesh(geometry_builder::create_cylinder(20, 20));
	//// - Sphere
	meshes["geomsp"] = mesh(geometry_builder::create_sphere(20, 20));
	//// - Torus
	meshes["geomto"] = mesh(geometry_builder::create_torus(20, 20, 1, 5));	
	// ***************************************
	// Set the transforms for your meshes here
	// ***************************************
	vec3 boxscale(5, 5, 5);
	vec3 boxpos(-10, 2.5, -30);
	meshes["geombx"].get_transform().scale = boxscale;
	meshes["geombx"].get_transform().position = boxpos;

	vec3 tetrascale(4, 4, 4);
	vec3 tetrapos(-30, 10, -10);
	meshes["geomtr"].get_transform().scale = tetrascale;
	meshes["geomtr"].get_transform().position = tetrapos;

	vec3 pryscale(5, 5, 5);
	vec3 prypos(-10, 7.5, -30);
	meshes["geompr"].get_transform().scale = pryscale;
	meshes["geompr"].get_transform().position = prypos;

	vec3 dkscale(3, 1, 3);
	vec3 dkpos(-10, 11.5, -30);
	meshes["geomdk"].get_transform().scale = dkscale;
	meshes["geomdk"].get_transform().position = dkpos;
	meshes["geomdk"].get_transform().rotate(angleAxis(pi<float>()/2, vec3(1, 0, 0)));

	vec3 clscale(5, 5, 5);
	vec3 clpos(-25, 2.5, -25);
	meshes["geomcl"].get_transform().scale = clscale;
	meshes["geomcl"].get_transform().position = clpos;

	vec3 spscale(2.5, 2.5, 2.5);
	vec3 sppos(-25, 10, -25);
	meshes["geomsp"].get_transform().scale = spscale;
	meshes["geomsp"].get_transform().position = sppos;

	vec3 topos(-25, 10, -25);
	meshes["geomto"].get_transform().position = topos;
	meshes["geomto"].get_transform().rotate(angleAxis(pi<float>() / 2, vec3(1, 0, 0)));

	// Load texture
	tex = texture("..\\resources\\textures\\checked.gif");

	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER); 
	// Build effect
	eff.build();

	// Set camera properties
	cam.set_position(vec3(10.0f, 10.0f, 10.0f));
	cam.set_target(vec3(-100.0f, 0.0f, -100.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}

bool update(float delta_time)
{
	// Update the camera
	cam.update(delta_time);
	return true;
}

bool render()
{
	// Render meshes
	for (auto &e : meshes)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(eff);
		// Create MVP matrix
		auto M = m.get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind and set texture
		renderer::bind(tex, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);

		// Render mesh
		renderer::render(m);
	}

	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
}